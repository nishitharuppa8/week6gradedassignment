package com.hcl.junit;

import static org.junit.jupiter.api.Assertions.*;
import java.sql.Connection;



import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.hcl.databases.DBConnection;

class Testclass {
	private Connection connection=null;
	//connection building
	@BeforeEach
	public void connectionFix() {
		this.connection=DBConnection.getInstance().getConnection();
	}
	// Testing for connection
	@Test
	public void connectionTest() {
		assertNotNull(connection);
	}
}