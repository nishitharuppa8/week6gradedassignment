package com.hcl.databases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	private static DBConnection instance;
	private static Connection conn;
	private DBConnection() {

	}
	public static DBConnection getInstance() {
		//Singleton design pattern for database connection
		if(instance == null) {
			instance = new DBConnection();
		}
		return instance;
	}
	public Connection getConnection() {	
		//if database is not connected, it creates a new connection else returns existing one
		if(conn == null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				conn = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/ecommerce","root","Nishi@123");
				System.out.println("Driver is Loaded and Connected");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println("Error....! Driver not loaded");
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("Cannot connect to database");
				e.printStackTrace();
			}
		}
		return conn;	
	}

}