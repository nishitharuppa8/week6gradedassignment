package com.hcl.factoryDP;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hcl.databases.DBConnection;
import com.hcl.model.Movie;

public class ComingSoon implements Classification{

	Connection con =DBConnection.getInstance().getConnection();
	// Connection Establishing

	public ComingSoon() {

	}
	@Override
	public List<Movie> movieType() throws SQLException {

		//To display movies
		List<Movie> movies=new ArrayList<Movie>();
		
		//Query to get data 
		String sql="select id,title,year,storyline,imdbRating,classification from moviedataset where Classification ='coming soon'";
		Statement statement =con.createStatement();
		ResultSet resultSet = statement.executeQuery(sql);
		while(resultSet.next())
		{

			Movie movie = new Movie();
			movie.setId(resultSet.getInt(1));
			movie.setTitle(resultSet.getString(2));
			movie.setYear(resultSet.getInt(3));
			movie.setStoryline(resultSet.getString(4));
			movie.setImdbRating(resultSet.getInt(5));
			movie.setClassification(resultSet.getString(6));
			movies.add(movie);
		}

		return movies;	
	}

}

