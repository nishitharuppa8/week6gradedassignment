package com.hcl.ui;


import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import com.hcl.factoryDP.Category;
import com.hcl.factoryDP.Classification;
import com.hcl.model.Movie;


public class Main {

	public static void main(String[] args) {
		int choice;


		Scanner scanner = new Scanner(System.in);
		do
		{

			System.out.println("WELCOME");
			System.out.println("Please select a category");
			System.out.println(
					"1)Coming Soon movies\n"
							+ "2)Movies In Theaters\n"
							+ "3)Top Rated Indian\n"
							+ "4)Top Rated Movies\n"
							+ "5) to exit");
			System.out.println("Enter your choice");
			System.out.println();

			choice = scanner.nextInt();

			switch(choice) {
			case 1:
				Classification comingSoon=Category.setMovieType(1);
				try {
					List<Movie> movie=comingSoon.movieType();
					System.out.println("*******Coming Soon Movies********");
					for(Movie m:movie) {
						System.out.println(m+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;
			case 2:
				Classification inTheaters=Category.setMovieType(2);
				try {
					List<Movie> movie=inTheaters.movieType();
					System.out.println("*******Movies in Theaters********");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;
			case 3:
				Classification topRatedInIndia=Category.setMovieType(3);
				try {
					List<Movie> movie=topRatedInIndia.movieType();
					System.out.println("*******Top Rated Indian*******");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;
			case 4:
				Classification topRated=Category.setMovieType(4);
				try {
					List<Movie> movie=topRated.movieType();
					System.out.println("*******Top Rated Movies********");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;

			case 5:
				System.exit(0);
				break;
			default:
				System.out.println("Not a Valid Choice...!");
				break;
			}
		}while(choice!=5);
	}

}
